import urllib2
import json
from urlparse import urlparse
from bs4 import BeautifulSoup


class ChatParser(object):

    def is_mention(self, suspect):
        """See if the suspect (string) is a mention. A mention always starts
        with an '@' and ends when hitting a non-word character

        :param suspect (string)
        :return (boolean)
        """
        return suspect.startswith('@')


    def is_emoticon(self, suspect):
        """See if the suspect is an emoticon. Emotics are ASCII strings, no
        longer than 15 characters, contained in parenthesis.

        :param suspect (string)
        :return (boolean)
        """
        max_length = 17
        return suspect.startswith('(') and suspect.endswith(')') and len(suspect) <= max_length


    def is_url(self, suspect):
        """See if the suspect is a valid URL. XXX Currently very narrow.
        Defining what makes a url 'valid' is pretty broad.

        :param suspect (string)
        :return (boolean)
        """
        return suspect.startswith('https://') or suspect.startswith('http://')


    def get_url_data(self, url):
        """Get the data for the url. Currently this is just the title

        :param url (string)
        :return (dict)
        """
        resp = urllib2.urlopen(url).read()
        title = BeautifulSoup(resp).title.string
        url_parts = urlparse(url)

        if url_parts.netloc == 'twitter.com':
            username = url_parts.path.split('/')[1]
            title = self.parse_twitter_title(username, title)

        return {
            'url': url,
            'title': title
        }


    def parse_twitter_title(self, username, title):
        """Regular Twitter tiles seem to be differently formatted. Format them

        :param username (string)
        :param title (string)
        :return (string)
        """
        tweet = title.split(':')[1].strip()
        truncated = tweet[1:27] # arbitrary truncation numbers
        summarized = 'Twitter / %s: %s ...' % (username, truncated)

        return summarized


    def parse(self, message):
        """Get any mentions, emoticons, or links with title in the message and
        return them as a json string

        :param message (string)
        :return (dict)
        """
        parts = message.split()
        parsed = {
            'mentions': [],
            'emoticons': [],
            'links': []
        }

        for part in parts:
            if self.is_mention(part):
                parsed['mentions'].append(part[1:]) #strip off '@'

            elif self.is_emoticon(part):
                parsed['emoticons'].append(part[1:len(part)-1]) #strip off ()

            elif self.is_url(part):
                try:
                    parsed['links'].append(self.get_url_data(part))
                except Exception as e:
                    print 'EXCEPTION %s' % e
                    pass

        return parsed


    def parse_to_json(self, message):
        """Get the parsed data as a JSON string

        :param message (string)
        :return (string)
        """
        return json.dumps(self.parse(message))
