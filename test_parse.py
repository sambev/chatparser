import unittest
import json
from chatparser import ChatParser

class TestChatParse(unittest.TestCase):

    def setUp(self):
        self.chatparser = ChatParser()

    def tearDown(self):
        pass

    def testIsMention(self):
        self.assertTrue(self.chatparser.is_mention('@chris'))
        self.assertFalse(self.chatparser.is_mention('chris'))

    def testIsEmoticon(self):
        self.assertTrue(self.chatparser.is_emoticon('(megusta)'))
        self.assertTrue(self.chatparser.is_emoticon('(coffee)'))
        self.assertTrue(self.chatparser.is_emoticon('(exactlyfifteene)'))

        self.assertFalse(self.chatparser.is_emoticon('(longerthanfiftee)'))

    def testIsUrl(self):
        self.assertTrue(self.chatparser.is_url('http://www.sambev.com'))

        self.assertFalse(self.chatparser.is_url('www.sambev.com'))
        self.assertFalse(self.chatparser.is_url('sambev.com'))
        self.assertFalse(self.chatparser.is_url('www.bleh'))

    def testMentions(self):
        message = '@chris you around? @sam needs help'
        parsed = json.loads(self.chatparser.parse_to_json(message))

        self.assertTrue('chris' in parsed['mentions'])
        self.assertTrue('sam' in parsed['mentions'])

    def testEmoticons(self):
        message = 'Good morning! (megusta) (coffee)'
        parsed = json.loads(self.chatparser.parse_to_json(message))

        self.assertTrue('megusta' in parsed['emoticons'])
        self.assertTrue('coffee' in parsed['emoticons'])

    def testUrls(self):
        message = 'Olympics are starting soon; http://www.nbcolympics.com'
        parsed = json.loads(self.chatparser.parse_to_json(message))
        values = parsed['links'][0].values()
        url = 'http://www.nbcolympics.com'
        title = 'NBC Olympics | Home of the 2016 Olympic Games in Rio'

        self.assertTrue(url in values)
        self.assertTrue(title in values)

    def testTwitter(self):
        title = 'Justin Dorfman on Twitter: "nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq"'
        parsed = self.chatparser.parse_twitter_title('jdorfman', title)

        self.assertEquals('Twitter / jdorfman: nice @littlebigdetail from ...', parsed)

    def testAll(self):
        message = '@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016'
        parsed = json.loads(self.chatparser.parse_to_json(message))
        link = parsed['links'][0]

        self.assertTrue('bob' in parsed['mentions'])
        self.assertTrue('john' in parsed['mentions'])
        self.assertTrue('success' in parsed['emoticons'])
        self.assertEquals(link['url'], 'https://twitter.com/jdorfman/status/430511497475670016')
        self.assertEquals(link['title'], 'Twitter / jdorfman: nice @littlebigdetail from ...')

if __name__ == '__main__':
    unittest.main()
